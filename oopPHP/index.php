    <?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');


    $sheep = new Animal("shaun");
    echo "Name : " . $sheep->name . "<br>";
    echo "legs : " . $sheep->legs . "<br>";
    echo "cold blooded : " . $sheep->coldblooded . "<br>";

    echo "<br>";

    $frog = new Animal("buduk");
    echo "Name : " . $frog->name . "<br>";
    echo "legs : " . $frog->legs . "<br>";
    echo "cold blooded : " . $frog->coldblooded . "<br>";
    echo "jump : ". $frog->jump;

    echo "<br>";
    
    $ape = new Animal("kera sakti");
    echo "Name : " . $ape->name . "<br>";
    echo "legs : " . $ape->legs . "<br>";
    echo "cold blooded : " . $ape->coldblooded . "<br>";
    
    $frog->jump();
    $ape->yell();
    
    
    ?>
